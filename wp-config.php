<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'q,&n<mA<<82^by(vRN^T1!)BR~wWK=+<[#ImML#~n{gyUV:ff:C/3N5T*er+iY5]' );
define( 'SECURE_AUTH_KEY',  'E.gp*td(K9*`2zib:j*W&{*V, juXLs847+iK+<]Xb?G`@KkLB=KN7.f&q1(|IH~' );
define( 'LOGGED_IN_KEY',    '}.&y$Bs;?CR_Ev!Y&?~U;,EVlStP_Y tHBrtcf4?1UHya:i7wTniJCR`/<kcv-,9' );
define( 'NONCE_KEY',        '!/.Fvt%o4ANT}mz!A_ayPU$%@71aW], g$(r3!8C5|Yv}S7$|~I{f75v 47K1-#(' );
define( 'AUTH_SALT',        '4)US9,Pkj{->-Pr]DBtY.c>y<Bd2Zz{soQ.7bq:4Ek`~Q,VbvHr^aPp{jB%=0S )' );
define( 'SECURE_AUTH_SALT', 'LIBDQmXNRj<H1.dE]}dcqAeDkh^/`[5Cd~k^aFiUkCf!wIl:kDUUYj6x E,^|DQR' );
define( 'LOGGED_IN_SALT',   'VXrKRcV@)[z;`M?>cY>c.vrrZw/tpgJHCNR+08R*bY^T/B6#!l{lx~p]E[<?SB-4' );
define( 'NONCE_SALT',       '>}9YwooAQr(_;[=m;n~GjnAl]Wzt]=pBw}LwQtOpy35LR-MO:60EEp1FmA$.fwTg' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';

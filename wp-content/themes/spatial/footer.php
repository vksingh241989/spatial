
<footer>

    <div class="footer-watermark">

        <img src="<?php bloginfo('template_url');?>/images/locations.png" class="w-100 d-block">

    </div>

    <div class="container position-relative">

        <div class="row">

            <div class="col-sm-12 col-xl-4 pe-lg-5">

                <h4>About Us</h4>

                <p>

                    Lorem ipsum dolor sit amet consec tetur adipi sicing elit. Non tempora eos, nam optio quo in placeat quisquam tempore exercit ationem nam quisquam tempore exercit volup tatum ab dolores deserunt optio quo in ationem ea? Lauda ntium, porro minus, nostrum eius magni volup tatum ab dolores deserunt optio quo in placeat volup tatum ab dolores deserunt optio quo in! <a href="#" data-bs-toggle="tooltip" title="Read more">Read more...</a>

                </p>

            </div>

            <div class="col-sm-12 col-xl-8">

                <div class="row">

                    <div class="col-6 col-md-4 col-lg-3 col-xl-3">

                        <h4>Company</h4>
                        <?php 
                            $defaults = array(
                                'numberposts'      => 10,
                                'category'         => 8,
                                'orderby'          => 'menu_order',
                                'order'            => 'DESC',
                                'include'          => array(),
                                'exclude'          => array('post_title'),
                                'meta_key'         => '',
                                'meta_value'       => '',
                                'post_type'        => 'page',
                                'suppress_filters' => true,
                            );
                         $myPages = get_posts( $defaults );
                        ?>

                        <ul class="footer-links">
                            <?php if(!empty($myPages)){
                               foreach ($myPages as $key => $value) { 
                                $value=(Array)$value;
                               if($value['post_parent']==0){ ?>

                                <li><a href="<?= $value['post_name'] ?>"><?= $value['post_title'] ?></a></li>

                            <?php }}} ?>    

                        </ul>

                    </div>

                    <div class="col-6 col-md-4 col-lg-3 col-xl-3">

                        <h4>Support</h4>

                        <?php 
                            $defaults = array(
                                'numberposts'      => 10,
                                'category'         => 9,
                                'orderby'          => 'menu_order',
                                'order'            => 'DESC',
                                'include'          => array(),
                                'exclude'          => array('post_title'),
                                'meta_key'         => '',
                                'meta_value'       => '',
                                'post_type'        => 'page',
                                'suppress_filters' => true,
                            );
                         $myPages = get_posts( $defaults );
                        ?>

                        <ul class="footer-links">
                            <?php if(!empty($myPages)){
                               foreach ($myPages as $key => $value) { 
                                $value=(Array)$value;
                               if($value['post_parent']==0){ ?>

                                <li><a href="<?= $value['post_name'] ?>"><?= $value['post_title'] ?></a></li>

                            <?php }}} ?>    

                        </ul>

                    </div>

                    <div class="col-sm-6 col-md-4 col-lg-3 col-xl-3">

                        <h4>Address</h4>

                        <address>

                            <a href="#">

                                <svg class="icon"><use href="icons.svg#icon_mapmarker"></use></svg>

                                425 Smith Street Fitzroy VIC 3065 Melbourne, Australia

                            </a>

                            <a href="mailto:info@spatialdistillery.com">

                                <svg class="icon"><use href="icons.svg#icon_envelope"></use></svg>

                                info@spatialdistillery.com

                            </a>

                            <a href="tel:+61439855117" title="Call">

                                <svg class="icon"><use href="icons.svg#icon_call"></use></svg>

                                <div class="w-100"><span class="d-block d-md-inline">Sydney Office:</span> +61 439 855 117</div>

                            </a>

                        </address>

                    </div>

                    <div class="col-sm-6 col-lg-3 col-xl-3">

                        <h4>Social Media</h4>

                        <ul class="list-inline footer-links">

                            <li class="list-inline-item">

                                <a href="#" class="fs-3 lh-1">

                                    <svg class="icon"><use href="icons.svg#icon_facebook"></use></svg>

                                </a>

                            </li>

                            <li class="list-inline-item">

                                <a href="#" class="fs-3 lh-1">

                                    <svg class="icon"><use href="icons.svg#icon_instagram"></use></svg>

                                </a>

                            </li>

                            <li class="list-inline-item">

                                <a href="#" class="fs-3 lh-1">

                                    <svg class="icon"><use href="icons.svg#icon_twitter"></use></svg>

                                </a>

                            </li>

                            <li class="list-inline-item">

                                <a href="#" class="fs-3 lh-1">

                                    <svg class="icon"><use href="icons.svg#icon_linkedin"></use></svg>

                                </a>

                            </li>

                        </ul>

                    </div>

                </div>

            </div>

        </div>

    </div>

</footer>



<script src="<?php bloginfo('template_url');?>/js/jquery-3.4.1.min.js"></script>

<script src="<?php bloginfo('template_url');?>/js/bootstrap.bundle.min.js"></script>

<script src="<?php bloginfo('template_url');?>/js/owl.carousel.min.js"></script>

<script>

$(document).ready(function(){

    $('#ourPartners, #ourCustomers').owlCarousel({

        items: 5,

        loop: true,

        margin: 10,

        autoplay: true,

        autoplayTimeout: 2000,

        autoplayHoverPause: true,

        nav: false,

        responsive:{

            0:{

                items: 2

            },

            600:{

                items: 3

            },

            1000:{

                items:4

            },

            1200:{

                items: 5

            }

        }

    });





    // Testimonials Carousel

    $('#testimonials').owlCarousel({

        center: true,

        items: 3,

        loop: true,

        margin: 0,

        nav: true,

        smartSpeed: 450,

        responsive:{

            0:{

                items: 1,

                nav: false,

                stagePadding: 0

            },

            640:{

                items: 2

            },

            1200:{

                items: 3

            }

        }

    });

});

</script>



<script>

/*Start | Text Rotating*/

var words = document.getElementsByClassName('word');

var wordArray = [];

var currentWord = 0;



words[currentWord].style.opacity = 1;

for (var i = 0; i < words.length; i++) {

    splitLetters(words[i]);

}

function changeWord() {

    var cw = wordArray[currentWord];

    var nw = currentWord == words.length-1 ? wordArray[0] : wordArray[currentWord+1];

    for (var i = 0; i < cw.length; i++) {

        animateLetterOut(cw, i);

    }

    for (var i = 0; i < nw.length; i++) {

        nw[i].className = 'letter behind';

        nw[0].parentElement.style.opacity = 1;

        animateLetterIn(nw, i);

    }

    currentWord = (currentWord == wordArray.length-1) ? 0 : currentWord+1;

}

function animateLetterOut(cw, i) {

  setTimeout(function() {

        cw[i].className = 'letter out';

  }, i*80);

}

function animateLetterIn(nw, i) {

  setTimeout(function() {

        nw[i].className = 'letter in';

  }, 340+(i*80));

}

function splitLetters(word) {

    var content = word.innerHTML;

    word.innerHTML = '';

    var letters = [];

    for (var i = 0; i < content.length; i++) {

        var letter = document.createElement('span');

        letter.className = 'letter';

        letter.innerHTML = content.charAt(i).replace(/ /g,'');

        word.appendChild(letter);

        letters.push(letter);

    }

    wordArray.push(letters);

}

changeWord();

setInterval(changeWord, 4000);

/*Start | Text Rotating*/

</script>



<script>

var menu = document.querySelector('.mobile-menu');

menu.addEventListener("click", function () {

    document.body.classList.toggle('active')

});







var timer;

var acc = [].slice.call(document.querySelectorAll('.accordion-button'));

acc.forEach(function (triggerEl) {

    triggerEl.addEventListener('mouseenter', function (event) {

        $('.accordion-button').removeClass('active');

        clearInterval(timer);

    });

    triggerEl.addEventListener('mouseleave', function (event) {

        timer = setInterval(collapseChanger, 5000);

        $('.accordion-button:not(.collapsed)').addClass('active');

    });

});

function collapseChanger() {

    if ($('.accordion-button:not(.collapsed)').parent().parent().next().find('.accordion-button').length>0) {

        $('.accordion-button').removeClass('active');

        $('.accordion-button:not(.collapsed)').parent().parent().next().find('.accordion-button').addClass('active');

        $('.accordion-button:not(.collapsed)').parent().parent().next().find('.accordion-button').trigger('click');

    }else{

        $('.accordion-button').removeClass('active');

        $('.accordion-item:nth-child(1)').find('.accordion-button').addClass('active');

        $('.accordion-item:nth-child(1)').find('.accordion-button').trigger('click');

    }

}

timer = setInterval(collapseChanger, 5000);

setTimeout(()=>{

    $('.accordion-button:not(.collapsed)').addClass('active');

},250);





var collapseElementList = [].slice.call(document.querySelectorAll('.collapse'))

var collapseList = collapseElementList.map(function (collapseEl) {

    // return new bootstrap.Collapse(collapseEl)

    collapseEl.addEventListener('show.bs.collapse', function (event) {

        $('.colhide').removeClass('active');

        $('.colhide-'+ $(event.target).attr('id')).addClass('active');

    });

});



$(window).scroll(function() {

    if ($(this).scrollTop() > 0) {

        $('header').addClass('active');

    } else {

        $('header').removeClass('active');

    }

});

</script>



</body>

</html>
<?php register_nav_menus(array('header' => 'header_menu', )); ?>
<?php add_theme_support('post-thumbnails');?>
<?php function wpb_custom_new_menu() {
  register_nav_menus(
    array(
      'my-custom-menu' => __( 'My Custom Menu' ),
      'extra-menu' => __( 'Extra Menu' )
    )
  );
}
add_action( 'init', 'wpb_custom_new_menu' ); ?>
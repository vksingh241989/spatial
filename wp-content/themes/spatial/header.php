<!doctype html>

<html lang="en">

<head>

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="<?php bloginfo('template_url');?>/css/bootstrap.min.css" rel="stylesheet">

    <link href="<?php bloginfo('template_url');?>/css/owl.carousel.min.css" rel="stylesheet">

    <link href="<?php bloginfo('template_url');?>/style.css?v=11" rel="stylesheet">

    <title>Home</title>

    <link href="favicon.ico" type="image/x-icon" rel="shortcut icon">

    <meta name="msapplication-TileColor" content="#1A512E">

    <meta name="theme-color" content="#1A512E">

    <meta name='apple-mobile-web-app-capable' content='yes'>

    <meta name="apple-mobile-web-app-status-bar-style" content="#1A512E">

</head>

<body>

<?php get_sidebar('nav');?>   
<header>

    <div class="container">

        <div class="row align-items-center justify-content-between">

            <div class="col col-lg-auto col-xxl mobile-row">

                <a href="#" class="logo-brand" title="Canverse">

                    <img src="<?php bloginfo('template_url');?>/images/logo.png?v=2" alt="Canverse"/>

                </a>

                <button class="mobile-menu">

                    <!-- <svg class="icon icon-4x"><use href="icons.svg#icon_menu"></use></svg> -->
                   <img src="https://img.icons8.com/material-outlined/24/000000/menu--v1.png"/>

                </button>

            </div>

            <div class="col-auto col-xl-auto for-mobile">

                <?php wp_nav_menu(array('container_id' => '','container'=>'ul','container_class'=>'', 'menu_id' => '', 'menu_class' =>'nav-item nav justify-content-center text-center', 'theme_location' => 'header' )); ?>  

                <?php
/*wp_nav_menu( array( 
    'theme_location' => 'my-custom-menu', 
    'container_class' => 'custom-menu-class' ) );

wp_nav_menu( array( 
    'theme_location' => 'extra-menu', 
    'container_class' => 'extra-menu-class' ) ); */
?>

               <!--  <ul class="nav justify-content-center text-center">

                    <li class="nav-item">

                        <a href="#" class="nav-link" href="#">Home</a>

                    </li>                    

                    <li class="nav-item position-relative">

                        <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown" data-bs-auto-close="inside">Products</a>

                        <ul class="dropdown-menu dropdown-menu-center shadow border-0">

                            <li><a href="#" class="dropdown-item">Technology</a></li>

                            <li><a href="#" class="dropdown-item">Data</a></li>

                            <li><a href="#" class="dropdown-item">Services</a></li>

                        </ul>

                    </li>

                    <li class="nav-item position-relative">

                        <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown" data-bs-auto-close="inside">Solutions</a>

                        <ul class="dropdown-menu dropdown-menu-center shadow border-0">

                            <li><a href="#" class="dropdown-item">By Industry</a></li>

                            <li><a href="#" class="dropdown-item">By Use Case</a></li>

                        </ul>

                    </li>

                    <li class="nav-item position-relative">

                        <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown" data-bs-auto-close="inside">Company</a>

                        <ul class="dropdown-menu dropdown-menu-center shadow border-0">

                            <li><a href="#" class="dropdown-item">Blog</a></li>

                            <li><a href="#" class="dropdown-item">About Us</a></li>

                            <li><a href="#" class="dropdown-item">Case Studies</a></li>

                            <li><a href="#" class="dropdown-item">Partners</a></li>

                            <li><a href="#" class="dropdown-item">Team</a></li>

                            <li><a href="#" class="dropdown-item">Press</a></li>

                            <li><a href="#" class="dropdown-item">Careers</a></li>

                        </ul>

                    </li>

                </ul> -->

            </div>

            <div class="col-md text-center text-lg-end button_div">

                <a href="#" class="btn btn-outline-light fw-bold px-3 btn_b">Login</a>

                <a href="#" class="btn btn-light fw-bold px-3 ms-3 btn_b" type="button">Book a Demo</a>

            </div>

        </div>

    </div>

</header>